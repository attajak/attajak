# [@attajak](https://gitlab.com/attajak/attajak)

[![@Attajak on Replit](https://replit.com/badge?caption=@Attajak%20on%20Replit)](https://replit.com/@attajak)

## Welcome to Attajak Janrak

**ชื่อ** คิง **เกิด** ๘ สิงหาฯ **เลือดกรุ๊ป** เอ

**ชอบ**_ดูหนัง-อนิเมะ_ _ฟังเพลง_ _เล่นเกม_

[![attajak](https://attajak.github.io/assets/images/attajak.jpg)](https://attajak.github.io)

### Follow or Contact me

Platform | Username/Links
--- | ---
GitHub: | [attajak](https://github.com/attajak)
GitHub Pages | <https://attajak.github.io>
GitLab: | [attajak](https://gitlab.com/attajak)
GitLab Pages: | <https://attajak.gitlab.io>
CloudFlare Pages: | <https://attajak.pages.dev>
Replit: | [@attajak](https://replit.com/@attajak)
Facebook: | [attajak](https://facebook.com/attajak)
Twitter: | [@attajak](https://twitter.com/attajak)
Fediverse | [@attajak@mastodon.in.th](https://mastodon.in.th/@attajak)
Threads: | [@attajak](https://www.threads.net/@attajak)
Instagram: | [attajak](https://instagram.com/attajak)
YouTube: | [@attajak](https://youtube.com/@attajak)
Linkedin: | [attajak](https:/linkedin.com/in/attajak)
WordPress: | <https://attajak.wordpress.com>
Blogger | <https://attajak.blogspot.com>
Gmail: | [Janrak at Gmail.Com](mailto:janrak@gmail.com)
Outlook: | [Attajak at Outlook.Com](mailto:attajak@outlook.com)

---

### Quotes

> แค่อยากทำหน้าที่ให้ดีที่สุด ไม่ได้อยากเป็นวีรบุรุษของใคร

> งานมีขี้บ่อย งานน้อยไม่ค่อยขี้ งานไม่มีไม่ขี้เลย

---

- 👋 Hi, I’m @attajak
- 👀 I’m interested in ...
- 🌱 I’m currently learning ...
- 💞️ I’m looking to collaborate on ...
- 📫 How to reach me ... [Janrak at Gmail dot Com](janrak@gmail.com)

<!---
attajak/attajak is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
